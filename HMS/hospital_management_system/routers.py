class HospitalManagementSystemDatabaseRouter(object):

    def db_for_read(self, model, **hints):
        if model._meta.app_label == 'hospital_management_system':
            return 'hospital_management_system'
        return None

    def db_for_write(self, model, **hints):
        if model._meta.app_label == 'hospital_management_system':
            return 'hospital_management_system'
        return None

    def allow_migrate(self,db,app_label,model_name=None, **hints):
        if app_label == 'hospital_management_system':
            return db=='hospital_management_system'
        elif db == 'hospital_management_system':
            return False

        return None