import os

from dotenv import load_dotenv
from pathlib import Path



env_path = Path('.') / '.env'
load_dotenv(dotenv_path=env_path)

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

SECRET_KEY = os.getenv('AUTH_SECRET_KEY')

JWT_SECRET_KEY = os.getenv('JWT_SECRET')
JWT_ALGORITHM = os.getenv('JWT_ALGORITHM')

SALT = os.getenv('SALT')
HASHER = os.getenv('HASHER')

DEBUG = True

ALLOWED_HOSTS = []

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'user_service',
    'hospital_management_system',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'HMS.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.media',
            ],
        },
    },
]

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
MAILER_EMAIL_BACKEND = EMAIL_BACKEND
EMAIL_HOST = os.getenv('EMAIL_HOST')
EMAIL_HOST_USER = os.getenv('EMAIL_USERNAME')
EMAIL_HOST_PASSWORD = os.getenv('EMAIL_PASSWORD')
EMAIL_PORT = 587
EMAIL_USE_TLS = True

WSGI_APPLICATION = 'HMS.wsgi.application'

DATABASE_ROUTERS = [
    'user_service.routers.UserServiceDatabaceRouter',
    'hospital_management_system.routers.HospitalManagementSystemDatabaseRouter',
    ]
DATABASE_APPS_MAPPING = {
    'user_service': 'user_service',
    'hospital_management_system': 'hospital_management_system',
}

DATABASES = {
    'default': {
        'ENGINE': os.getenv('DB_ENGINE'),
        'NAME' : os.getenv('DB_NAME'),
        'HOST': os.getenv('DB_HOST'),
        'PORT' : os.getenv('DB_PORT'),
        'USER' : os.getenv('DB_USERNAME'),
        'PASSWORD' : os.getenv('DB_PASSWORD'),
    },
    'user_service': {
        'ENGINE': os.getenv('DB_ENGINE_USER_SERVICE'),
        'NAME' : os.getenv('DB_NAME_USER_SERVICE'),
        'HOST': os.getenv('DB_HOST_USER_SERVICE'),
        'PORT' : os.getenv('DB_PORT_USER_SERVICE'),
        'USER' : os.getenv('DB_USERNAME_USER_SERVICE'),
        'PASSWORD' : os.getenv('DB_PASSWORD_USER_SERVICE'),
    },
    'hospital_management_system': {
        'ENGINE': os.getenv('DB_ENGINE_HMS'),
        'NAME' : os.getenv('DB_NAME_HMS'),
        'HOST': os.getenv('DB_HOST_HMS'),
        'PORT' : os.getenv('DB_PORT_HMS'),
        'USER' : os.getenv('DB_USERNAME_HMS'),
        'PASSWORD' : os.getenv('DB_PASSWORD_HMS'),
    },
}

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'Asia/Kolkata'

USE_I18N = True

USE_L10N = True

USE_TZ = True

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static/')

MEDIA_ROOT = os.path.join(BASE_DIR, 'images/')
MEDIA_URL = '/images/'

LOGOUT_REDIRECT = "login.html"