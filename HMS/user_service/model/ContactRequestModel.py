from django.db import models

class ContactRequest(models.Model):

    name        =   models.IntegerField(blank=True)
    email       =   models.CharField(max_length=100, null=True)
    phone       =   models.CharField(max_length=13, null=True)
    details     =   models.TextField(blank=True)
    created_at  =   models.DateTimeField(auto_now_add=True, null=True)

    class Meta:
        db_table = "contact_request"