from django.db import models
from user_service.models import Login

class LoginHistory(models.Model):

    login        =   models.ForeignKey(Login, on_delete=models.CASCADE, unique=False)
    login_at     =   models.DateTimeField(auto_now_add=True, null=True)

    objects = models.Manager()


    class Meta:
        db_table = "login_history"