class UserServiceDatabaceRouter(object):

    def db_for_read(self, model, **hints):
        if model._meta.app_label == 'user_service':
            return 'user_service'
        return None

    def db_for_write(self, model, **hints):
        if model._meta.app_label == 'user_service':
            return 'user_service'
        return None

    def db_relation(self,obj1,obj2, **hints):

        if obj1._meta.app_label == 'user_service' and obj2._meta.app_label == 'user_service':
            return True
        return None